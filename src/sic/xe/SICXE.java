package sic.xe;

import sic.xe.assembler.Assembler;

public class SICXE
{
    public static String machineName;
    public static int wordSize;
    public static int mainMemoryBytes;
    public static ValuesHandler vh;
    public static MacroExpander macroExpander;
    public static Assembler assembler;
    public static Machine machine;
    public static Interface gui;

    public static void restart()
    {
        SICXE.vh = new ValuesHandler();
        SICXE.macroExpander = new MacroExpander();
        SICXE.machine = new Machine();
        SICXE.gui = new Interface();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        SICXE.machineName = "SIC/XE";
        SICXE.wordSize = 24;
        SICXE.mainMemoryBytes = 1048576;
        SICXE.vh = new ValuesHandler();
        SICXE.macroExpander = new MacroExpander();
        //SICXE.assembler = new Assembler(); // inicializado cada vez que for necessário
        SICXE.machine = new Machine();

        java.awt.EventQueue.invokeLater(() ->
        {
            SICXE.gui = new Interface();
        });
    }
}
