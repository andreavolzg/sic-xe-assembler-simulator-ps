package sic.xe;

public class ValuesHandler
{
    private final int representation_limit;
    private final int representation_upper_limit;
    private final int representation_lower_limit;
    private final int word_difference;
    private final String bin_format;

    public ValuesHandler()
    {
        this.representation_limit = ((int) Math.pow(2.0, SICXE.wordSize) - 1);
        this.representation_upper_limit = ((int) Math.pow(2.0, SICXE.wordSize - 1) - 1);
        this.representation_lower_limit = (this.representation_upper_limit + 1) * -1;
        this.word_difference = 32 - SICXE.wordSize;
        this.bin_format = "%" + SICXE.wordSize + "s";
    }

    public int maxRepresentableUnsigned()
    {
        return this.representation_limit;
    }

    public int maxRepresentableSigned()
    {
        return this.representation_upper_limit;
    }

    public int minRepresentableSigned()
    {
        return this.representation_lower_limit;
    }

    public int unsignedRepresentability(int value)
    {
        if (value > this.representation_limit)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public int signedRepresentability(int value)
    {
        if (value < this.representation_lower_limit)
        {
            return -1;
        }
        else if (value > this.representation_upper_limit)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public int wordToRealWord(int word)
    {
        return ((word << this.word_difference) / 256);
    }

    public int wordFromBytes(byte... bytes)
    {
        int word;

        if (bytes[0] < 0)
        {
            word = -1;

            for (int i = bytes.length - 1, shiftFactor = 0; i >= 0; i--, shiftFactor += 8)
            {
                word &= (bytes[i] << shiftFactor);
                if ((bytes.length - i) >= (SICXE.wordSize / 8))
                {
                    break;
                }
            }
        }
        else
        {
            word = 0;

            for (int i = bytes.length - 1, shiftFactor = 0; i >= 0; i--, shiftFactor += 8)
            {
                word |= (bytes[i] << shiftFactor);
                if ((bytes.length - i) >= (SICXE.wordSize / 8))
                {
                    break;
                }
            }
        }

        return word;
    }

    public byte wordNthByte(int word, int N)
    {
        if (N < 1)
        {
            N = 1;
        }
        else if (N > 4)
        {
            N = 4;
        }

        return (byte) (word << ((4 - N) * 8));
    }

    public String toBin(int value)
    {
        String buffer;

        if (value < 0)
        {
            buffer = String.format(this.bin_format, Integer.toString(Math.abs(value) - 1, 2));
            buffer = buffer.replace('1', 'X').replace(' ', '1').replace('0', '1');
            return buffer.replace('X', '0').substring(buffer.length() - SICXE.wordSize);
        }
        else
        {
            buffer = String.format(this.bin_format, Integer.toString(Math.abs(value), 2));
            return buffer.replace(' ', '0').substring(buffer.length() - SICXE.wordSize);
        }
    }

    public String toBin(int value, int bits)
    {
        String buffer;

        if (value < 0)
        {
            buffer = String.format("%" + Integer.toString(bits) + "s", Integer.toString(Math.abs(value) - 1, 2));
            buffer = buffer.replace('1', 'X').replace(' ', '1').replace('0', '1');
            return buffer.replace('X', '0').substring(buffer.length() - bits);
        }
        else
        {
            buffer = String.format("%" + Integer.toString(bits) + "s", Integer.toString(Math.abs(value), 2));
            return buffer.replace(' ', '0').substring(buffer.length() - bits);
        }
    }

    public String toHex(int value)
    {
        String buffer = "";

        for (int shiftFactor = SICXE.wordSize - 4; shiftFactor >= 0; shiftFactor -= 4)
        {
            buffer += Integer.toHexString((value >> shiftFactor) & 15);
        }

        return ("0x" + buffer);
    }

    public String toHex(int value, int digits)
    {
        String buffer = "";

        for (int shiftFactor = (digits * 8) - 4; shiftFactor >= 0; shiftFactor -= 4)
        {
            buffer += Integer.toHexString((value >> shiftFactor) & 15);
        }

        return ("0x" + buffer);
    }
}
