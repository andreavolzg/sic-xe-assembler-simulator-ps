package sic.xe;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Machine
{
    private final int[] registers;

    private final byte[] main_memory;

    private Queue<Integer> lastly_modified_mem_pos;
    private Queue<String> errors;

    private void enqueueError(String message)
    {
        this.errors.offer(Integer.toString(this.registers[7]) + ": " + message + "\n");
    }

    private int evaluateAddress(int word)
    {
        int address;

        if (((word & 16384) > 0) && ((word & 8192) == 0))
        {
            address = this.registers[3] + (word & 4095);
        }
        else if (((word & 16384) == 0) && ((word & 8192) > 0))
        {
            int signedDisplacement = (word & 4095);
            if ((signedDisplacement & 2048) > 0)
            {
                signedDisplacement |= -4096;
            }
            address = this.registers[7] + signedDisplacement;
        }
        else
        {
            address = (word & 4095);
        }

        return address;
    }

    private void compareAndSetCC(int val1, int val2)
    {
        if (val1 == val2)
        {
            this.registers[8] = 0B00000000000000000000000000000000;
        }
        else if (val1 > val2)
        {
            this.registers[8] = 0B00000000000000000000000000000001;
        }
        else
        {
            this.registers[8] = 0B00000000111111111111111111111111;
        }
    }

    private void runAccMemOperation(int opcode, int address)
    {
        switch (opcode)
        {
            case 6:
                this.registers[0] += this.memoryWordAt(address);
                break;
            case 7:
                this.registers[0] -= this.memoryWordAt(address);
                break;
            case 8:
                this.registers[0] *= this.memoryWordAt(address);
                break;
            case 9:
                if (this.memoryWordAt(address) == 0)
                {
                    this.enqueueError("Division by zero");
                    return;
                }
                this.registers[0] /= this.memoryWordAt(address);
                break;
            case 10:
                this.compareAndSetCC(this.registers[0], this.memoryWordAt(address));
                break;
            case 11:
                this.registers[1]++;
                this.compareAndSetCC(this.registers[1], this.memoryWordAt(address));
                break;
            case 16:
                this.registers[0] &= this.memoryWordAt(address);
                break;
            case 17:
                this.registers[0] |= this.memoryWordAt(address);
                break;
        }
    }

    private void runJumpOperation(int opcode, int address)
    {
        if ((opcode == 15)
            || ((opcode == 12) && (this.registers[8] == 0B00000000000000000000000000000000))
            || ((opcode == 13) && (this.registers[8] == 0B00000000000000000000000000000001))
            || ((opcode == 14) && (this.registers[8] == 0B00000000111111111111111111111111)))
        {
            this.registers[7] = address;
        }
        else if (opcode == 18)
        {
            this.registers[2] = this.registers[7];
            this.registers[7] = address;
        }
        else
        {
            this.registers[7] = this.registers[2];
        }
    }

    private void runLoadOperation(int opcode, int address)
    {
        int registerIndex;

        if ((opcode >= 0) && (opcode < 3))
        {
            registerIndex = opcode;
        }
        else if ((opcode > 25) && (opcode < 29))
        {
            registerIndex = opcode - 23;
        }
        else if (opcode == 52)
        {
            registerIndex = 8;
        }
        else
        {
            this.registers[0] = (int) (this.main_memory[address]) & 255;
            return;
        }

        this.registers[registerIndex] = this.memoryWordAt(address);
    }

    private void runStoreOperation(int opcode, int address)
    {
        int registerIndex;

        if ((opcode > 2) && (opcode < 6))
        {
            registerIndex = opcode - 3;
        }
        else if ((opcode > 29) && (opcode < 33))
        {
            registerIndex = opcode - 27;
        }
        else if (opcode == 58)
        {
            registerIndex = 8;
        }
        else
        {
            this.main_memory[address] = (byte) (this.registers[0] & 255);
            return;
        }

        this.setMemoryWordAt(this.registers[registerIndex], address);
    }

    private void runRegistersOperation(int opcode, int r1, int r2)
    {
        if (r2 > 8)
        {
            this.enqueueError("Trying to operate over non-existent register (r2 = " + Integer.toString(r2) + ")");
            return;
        }
        else if (r1 > 8)
        {
            this.enqueueError("Trying to operate over non-existent register (r1 = " + Integer.toString(r1) + ")");
            return;
        }
        else if (((opcode == 45) || (opcode == 46)) && (r2 != 0))
        {
            this.enqueueError("r2 specified but not expected");
        }

        switch (opcode)
        {
            case 36:
                this.registers[r2] += this.registers[r1];
                break;
            case 37:
                this.registers[r2] -= this.registers[r1];
                break;
            case 38:
                this.registers[r2] *= this.registers[r1];
                break;
            case 39:
                if (this.registers[r1] == 0)
                {
                    this.enqueueError("Division by zero");
                    return;
                }
                this.registers[r2] /= this.registers[r1];
                break;
            case 40:
                this.compareAndSetCC(this.registers[r1], this.registers[r2]);
                break;
            case 43:
                this.registers[r2] = this.registers[r1];
                break;
            case 45:
                this.registers[r1] = 0;
                break;
            case 46:
                this.registers[1]++;
                this.compareAndSetCC(this.registers[1], this.registers[r1]);
                break;
        }
    }

    private void runShiftOperation(int opcode, int r1, int n)
    {
        if (r1 > 8)
        {
            this.enqueueError("Trying to operate over non-existent register (r1 = " + Integer.toString(r1) + ")");
            return;
        }

        if (opcode == 41)
        {
            int buffer = this.registers[r1] >>> (24 - n);
            this.registers[r1] = (this.registers[r1] << (n + 1)) | buffer;
        }
        else
        {
            this.registers[r1] = (this.registers[r1] << 8) >> (8 + n);
        }
    }

    public Machine()
    {
        this.registers = new int[]
        {
            // A, X, L, B, S, T, F, PC, SW (CC)
            0, 0, 0, 0, 0, 0, 0, 0, 0
        };

        this.lastly_modified_mem_pos = new LinkedList<>();
        this.errors = new LinkedList<>();
        this.main_memory = new byte[SICXE.mainMemoryBytes];
        Arrays.fill(this.main_memory, (byte) 0);
    }

    public void runInstruction(int wordAddress)
    {
        int word = (wordAddress > 0) ? this.registers[7] : this.memoryWordAt(wordAddress);
        word &= SICXE.vh.maxRepresentableUnsigned();
        short opcode = (short) (word >> 18);
        int address = this.evaluateAddress(word);

        boolean flag_N = ((word & 131072) > 0), flag_I = ((word & 65536) > 0),
                flag_X = ((word & 32768) > 0);

        if (((opcode > 5) && (opcode < 12)) || (opcode == 16) || (opcode == 17))
        {
            this.runAccMemOperation(opcode, address);
        }
        else if (((opcode > 11) && (opcode < 16)) || (opcode == 18) || (opcode == 19))
        {
            this.runJumpOperation(opcode, address);
        }
        else if (((opcode >= 0) && (opcode < 3)) || (opcode == 20)
                 || ((opcode > 25) && (opcode < 29)) || (opcode == 52))
        {
            this.runLoadOperation(opcode, address);
        }
        else if (((opcode > 2) && (opcode < 6)) || (opcode == 21)
                 || ((opcode > 29) && (opcode < 33)) || (opcode == 58))
        {
            this.runStoreOperation(opcode, address);
        }
        else if (((opcode > 35) && (opcode < 41)) || (opcode == 43) || (opcode == 45) || (opcode == 46))
        {
            this.runRegistersOperation(opcode, ((word >> 12) & 15), ((word >> 8) & 15));
        }
        else if ((opcode == 42) || (opcode == 43))
        {
            this.runShiftOperation(opcode, ((word >> 12) & 15), ((word >> 8) & 15));
        }
        else
        {
            this.enqueueError("Unknown instruction (opcode = " + Integer.toString(opcode) + ")");
        }
    }

    public void runInstruction()
    {
        this.runInstruction(-1);
    }

    void run()
    {
        if ((this.registers[7] % 3) != 0)
        {
            this.registers[7] = this.registers[7] - (this.registers[7] % 3);
        }

        for (; this.registers[7] < (SICXE.mainMemoryBytes - 3); this.registers[7] += 3)
        {
            this.runInstruction(this.memoryWordAt(this.registers[7]));
            if (this.gotErrors())
            {
                break;
            }
        }

        for (String buffer = ""; !(".".equals(buffer = this.errorMessage()));)
        {
            System.err.println(buffer);
            System.exit(125);
        }

        this.lastly_modified_mem_pos.clear();
    }

    public int register(String regName)
    {
        regName = regName.toLowerCase().replaceAll("[\\-_.]", " ").replaceAll("\\(.*\\)", "");

        if (regName.matches("a(c(cumulator)?)?"))
        {
            return this.registers[0];
        }
        else if (regName.matches("(inde)?x( ?register)?"))
        {
            return this.registers[1];
        }
        else if (regName.matches("l(ink(age( ?register)?)?)?"))
        {
            return this.registers[2];
        }
        else if (regName.matches("b(ase( ?register)?)?"))
        {
            return this.registers[3];
        }
        else if (regName.matches("(general ?reg(ister)? ?)?s"))
        {
            return this.registers[4];
        }
        else if (regName.matches("(general ?reg(ister)? ?)?t"))
        {
            return this.registers[5];
        }
        else if (regName.matches("f(loat(ing ?point)?) ?(register|a(c(cumulator)?)?)?"))
        {
            return this.registers[6];
        }
        else if (regName.matches("p(rogram ?counter|c)"))
        {
            return this.registers[7];
        }
        else if (regName.matches("s(tatus ?word|w)"))
        {
            return this.registers[8];
        }
        else
        {
            return 16777216;
        }
    }

    public int PC()
    {
        return this.registers[7];
    }

    public int setRegister(String regName, int value)
    {
        regName = regName.toLowerCase().replaceAll("[\\-_.]", " ").replaceAll("\\(.*\\)", "");

        if (regName.matches("a(c(cumulator)?)?"))
        {
            this.registers[0] = value & SICXE.vh.maxRepresentableUnsigned();
        }
        else if (regName.matches("(inde)?x( ?register)?"))
        {
            this.registers[1] = value & SICXE.vh.maxRepresentableUnsigned();
        }
        else if (regName.matches("l(ink(age( ?register)?)?)?"))
        {
            this.registers[2] = value & SICXE.vh.maxRepresentableUnsigned();
        }
        else if (regName.matches("b(ase( ?register)?)?"))
        {
            this.registers[3] = value & SICXE.vh.maxRepresentableUnsigned();
        }
        else if (regName.matches("(general ?reg(ister)? ?)?s"))
        {
            this.registers[4] = value & SICXE.vh.maxRepresentableUnsigned();
        }
        else if (regName.matches("(general ?reg(ister)? ?)?t"))
        {
            this.registers[5] = value & SICXE.vh.maxRepresentableUnsigned();
        }
        else if (regName.matches("f(loat(ing ?point)?) ?(register|a(c(cumulator)?)?)?"))
        {
            this.registers[6] = value /* & (SICXE.vh.maxRepresentableUnsigned() * 2) */;
        }
        else if (regName.matches("p(rogram ?counter|c)"))
        {
            this.registers[7] = value & SICXE.vh.maxRepresentableUnsigned();
        }
        else if (regName.matches("s(tatus ?word|w)"))
        {
            this.registers[8] = value & SICXE.vh.maxRepresentableUnsigned();
        }

        System.out.println("## Register '" + regName + "' set to " + Integer.toString(value));

        return value;
    }

    public byte memoryByteAt(int pos)
    {
        if (pos >= SICXE.mainMemoryBytes)
        {
            System.out.println("Trying to access invalid memory position!");
            return this.main_memory[0];
        }
        else
        {
            return this.main_memory[pos];
        }
    }

    public int memoryWordAt(int pos)
    {
        pos -= (pos % (SICXE.wordSize / 8));

        if (pos >= SICXE.mainMemoryBytes)
        {
            System.out.println("Trying to access invalid memory position!");
            return SICXE.vh.wordFromBytes(this.main_memory[0],
                                          this.main_memory[1],
                                          this.main_memory[2]);
        }
        else
        {
            return SICXE.vh.wordFromBytes(this.main_memory[pos],
                                          this.main_memory[pos + 1],
                                          this.main_memory[pos + 2]);
        }
    }

    public byte setMemoryByteAt(byte value, int pos)
    {
        System.out.println("## Memory position '" + Integer.toString(pos) + "' set to " + Byte.toString(value));

        if (pos >= SICXE.mainMemoryBytes)
        {
            System.out.println("Trying to access invalid memory position!");
            return 0;
        }
        else
        {
            this.main_memory[pos] = value;

            this.lastly_modified_mem_pos.offer(pos);
            return this.main_memory[pos];
        }
    }

    public byte setMemoryWordAt(int word, int pos)
    {
        System.out.println("## Word at memory position '" + Integer.toString(pos)
                           + "' set to " + Integer.toString(word));

        pos -= (pos % (SICXE.wordSize / 8));

        if (pos >= SICXE.mainMemoryBytes)
        {
            System.out.println("Trying to access invalid memory position!");
            return 0;
        }
        else
        {
            this.main_memory[pos] = SICXE.vh.wordNthByte(word, 1);
            this.main_memory[pos + 1] = SICXE.vh.wordNthByte(word, 2);
            this.main_memory[pos + 2] = SICXE.vh.wordNthByte(word, 3);

            this.lastly_modified_mem_pos.offer(pos);
            return this.main_memory[pos];
        }

    }

    public void clearMemory()
    {
        for (int i = 0; i < SICXE.mainMemoryBytes; i++)
        {
            this.main_memory[i] = 0;
        }
    }

    public int modifiedMemoryPos()
    {
        if (this.lastly_modified_mem_pos.size() > 0)
        {
            return this.lastly_modified_mem_pos.remove();
        }
        else
        {
            return -1;
        }
    }

    public boolean gotMemoryModifications()
    {
        return (this.lastly_modified_mem_pos.size() > 0);
    }

    public String errorMessage()
    {
        if (this.errors.size() > 0)
        {
            return this.errors.remove();
        }
        else
        {
            return ".";
        }
    }

    public boolean gotErrors()
    {
        return (this.errors.size() > 0);
    }
}
