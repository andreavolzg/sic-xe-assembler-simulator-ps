package sic.xe;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MacroExpander
{
    private String[] arrayLinhas;
    private String[] arrayRDBUFF;
    private String[] arrayWRBUFF;
    private String[] arraySubsRDBUFF;
    private String[] arraySubsWRBUFF;
    private int contaLinhaAtual;
    private String label;
    private List linhas;
    private String[] arrayMain;
    private String[] arrayFinal;
    private List fim;
    private int tamanhoRDBUFF;
    private int tamanhoWRBUFF;

    /**
     * É o construtor da classe macro.
     */
    public MacroExpander()
    {
        contaLinhaAtual = 0;
        //tamanhoWRBUFF = 0;
        linhas = new ArrayList();
        fim = new ArrayList();
    }

    /**
     * Abre um arquivo e passa esse arquivo para um array de linhas.
     *
     * @param code É a String contendo o código
     * //@param caminho É o nome do arquivo de texto
     * @throws java.io.IOException
     */
    public void leEntrada(String code) throws IOException //, FileNotFoundException
    { // parâmetro renomeado de "caminho" para "code"
        InputStream is = new ByteArrayInputStream(code.getBytes()); // adicionado
        BufferedReader entrada = new BufferedReader(new InputStreamReader(is)); // adicionado
        //BufferedReader arquivo = new BufferedReader(new FileReader(caminho));
        String linhaAtual;
        //while ((linhaAtual = arquivo.readLine()) != null)
        while ((linhaAtual = entrada.readLine()) != null) // adicionado
        {
            linhas.add(linhaAtual);
        }
        String[] arrayLinha = new String[linhas.size()];
        linhas.toArray(arrayLinha);
        arrayLinhas = arrayLinha;
        //arquivo.close();
    }

    /**
     * Separa as chamadas WRBUFF e RDBUFF para sesus respectivos arrays
     */
    public void separaChamada()
    {
        int i = 0, j = 0, k = 0, l = 0, linhasRDBUFF = 0, linhasWRBUFF = 0;
        String[] posicao, posicaoChamada;
        for (i = 0; i < arrayLinhas.length; i++)
        {
            posicao = arrayLinhas[i].split("\t");
            if ((posicao[0].equals("RDBUFF")) && (posicao[1].equals("MACRO")))
            {
                //System.out.println("Achou a chamada de macro RDBUFF");
                linhasRDBUFF = i;
                for (j = i; j < arrayLinhas.length; j++)
                {
                    posicaoChamada = arrayLinhas[j].split("\t");
                    if (!posicaoChamada[1].equals("MEND"))
                    {
                        //System.out.println(posicaoChamada[1]);
                        linhasRDBUFF = linhasRDBUFF + 1;
                        //arrayRDBUFF[k] = arrayLinhas[j];
                        //System.out.println(arrayRDBUFF[k]);
                        k = k + 1;
                    }
                    else
                    {
                        k = k + 1;
                        j = arrayLinhas.length;
                    }
                    //System.out.println("RDBUFF: "+k);
                }
                int m = 0;
                arrayRDBUFF = new String[k];
                for (l = (linhasRDBUFF - k) + 1; l <= k; l++)
                {
                    arrayRDBUFF[m] = arrayLinhas[l];
                    linhas.remove(arrayLinhas[l]);
                    //System.out.println("Linha RDBUFF: "+arrayRDBUFF[m]);
                    m = m + 1;
                }
            }
            if ((posicao[0].equals("WRBUFF")) && (posicao[1].equals("MACRO")))
            {
                //System.out.println("Achou a chamada de macro WRBUFF");
                linhasWRBUFF = i;
                for (j = i; j < arrayLinhas.length; j++)
                {
                    posicaoChamada = arrayLinhas[j].split("\t");
                    if (!posicaoChamada[1].equals("MEND"))
                    {
                        //System.out.println(posicaoChamada[1]);
                        linhasWRBUFF = linhasWRBUFF + 1;
                        //arrayRDBUFF[k] = arrayLinhas[j];
                        //System.out.println(arrayRDBUFF[k]);
                        k = k + 1;
                    }
                    else
                    {
                        k = k + 1;
                        j = arrayLinhas.length;
                    }
                    //System.out.println("WRBUFF: "+k);
                }
                int m = 0;
                arrayWRBUFF = new String[k];
                for (l = ((linhasWRBUFF - k) + 1) + linhasRDBUFF; l <= k; l++)
                {
                    arrayWRBUFF[m] = arrayLinhas[l];
                    linhas.remove(arrayLinhas[l]);
                    //System.out.println("Linha WRBUFF: "+arrayWRBUFF[m]);
                    m = m + 1;
                }
            }
        }
        String[] lin = new String[linhas.size()];
        linhas.toArray(lin);
        arrayMain = lin;
        arraySubsRDBUFF = arrayRDBUFF;
        arraySubsWRBUFF = arrayWRBUFF;
    }

    public void imprimeMain()
    {
        int i = 0;
        for (i = 0; i < arrayMain.length; i++)
        {
            //System.out.println("Main: "+arrayMain[i]);
        }
    }

    public void imprimeWRBUFF()
    {
        int i = 0;
        for (i = 0; i < arrayWRBUFF.length; i++)
        {
            System.out.println("Aray WRBUFF: " + arrayWRBUFF[i]);
            tamanhoWRBUFF = tamanhoWRBUFF + 1;
            if (arrayWRBUFF[i] == null)
            {
                i = arrayWRBUFF.length;
            }

        }
        tamanhoWRBUFF = tamanhoWRBUFF - 1;
    }

    /**
     * Ajusta o tamanho do array WRBUFF
     */
    public void ajustaWRBUFF()
    {
        int i = 0;
        for (i = 0; i < arrayWRBUFF.length; i++)
        {
            //System.out.println("Aray WRBUFF: "+arrayWRBUFF[i]);
            tamanhoWRBUFF = tamanhoWRBUFF + 1;
            if (arrayWRBUFF[i] == null)
            {
                i = arrayWRBUFF.length;
            }

        }
        tamanhoWRBUFF = tamanhoWRBUFF - 1;
    }

    /**
     * Ajusta o tamanho do array RDBUFF
     */
    public void ajustaRDBUFF()
    {
        int i = 0;
        for (i = 0; i < arrayRDBUFF.length; i++)
        {
            //System.out.println("Aray WRBUFF: "+arrayWRBUFF[i]);
            tamanhoRDBUFF = tamanhoRDBUFF + 1;
            if (arrayRDBUFF[i] == null)
            {
                i = arrayRDBUFF.length;
            }

        }
        tamanhoRDBUFF = tamanhoRDBUFF - 1;
    }

    /**
     * Imprime o array RDBUFF
     */
    public void imprimeRDBUFF()
    {
        int i = 0;
        for (i = 0; i < arrayRDBUFF.length; i++)
        {
            System.out.println("Array RDBUFF: " + arrayRDBUFF[i]);
        }
    }

    /**
     * Imprime o array final
     */
    public String imprimeArrayFinal() // valor de retorno modificado de void para String
    {
        int i = 0;
        String buffer = ""; // adicionado
        for (i = 0; i < arrayFinal.length; i++)
        {
            //System.out.println("Array Final: "+arrayFinal[i]);
            //System.out.println(arrayFinal[i]);
            buffer += arrayFinal[i] + "\n"; // adicionado
        }
        return buffer; // adicionado
    }

    /**
     * @return Retorna o número de linhas que o i do for da procuraChamadaMacro
     * tem que voltar para procurar se tem macro dentro de macro.
     */
    public int contaMacro()
    {
        int numero = 1, i = 0, vezesRD = 0, vezesWR = 0;
        String[] posicao;
        for (i = 0; i < arrayMain.length; i++)
        { //Percorre todas linhas do arquivo, e quando encontra a chamada da macro, então chama a função expandir macro
            posicao = arrayMain[i].split("\t");
            contaLinhaAtual = contaLinhaAtual + 1;
            fim.add(arrayMain[i]);
            if ((posicao[1].equals("RDBUFF")) || (posicao[1].equals("RDREC")) || (posicao[1].equals("WRBUFF")) || (posicao[1].equals("WRREC")) || (posicao[1].equals("MACROX")))
            { //Encontrou uma chamada de macro
                //System.out.println("Chamada encontrada: "+posicao[1]);
                label = posicao[0];
                if (posicao[1].equals("RDBUFF"))
                {
                    vezesRD = vezesRD + 1;
                }
                if (posicao[1].equals("WRBUFF"))
                {
                    vezesWR = vezesWR + 1;
                }
            }
        }
        if (vezesRD == 0)
        {
            vezesRD = 1;
        }
        if (vezesWR == 0)
        {
            vezesWR = 1;
        }
        vezesRD = vezesRD * (tamanhoRDBUFF - 2);
        vezesWR = vezesWR * (tamanhoWRBUFF - 2);
        numero = vezesRD + vezesWR + arrayMain.length + 1;
        return numero;
    }

    /**
     * Percorre o arquivo procurando onde está a primeira chamada da macro
     */
    public void procuraChamadaDeMacro()
    {
        int i = 0, j = 0, k = 0, l = 0;
        String[] posicao;
        ajustaWRBUFF();
        ajustaRDBUFF();
        //l = contaMacro();
        //System.out.println("LLLL: "+l);
        for (i = 0; i < arrayMain.length; i++)
        { //Percorre todas linhas do arquivo, e quando encontra a chamada da macro, então chama a função expandir macro
            //System.out.println("i for: "+i);
            posicao = arrayMain[i].split("\t");
            contaLinhaAtual = contaLinhaAtual + 1;
            fim.add(arrayMain[i]);
            if ((posicao[1].equals("RDBUFF")) || (posicao[1].equals("RDREC")) || (posicao[1].equals("WRBUFF")) || (posicao[1].equals("WRREC")) || (posicao[1].equals("MACROX")))
            { //Encontrou uma chamada de macro
                //System.out.println("Chamada encontrada: "+posicao[1]);
                label = posicao[0];
                //------Aqui
                if (posicao[1].equals("RDBUFF"))
                {
                    // System.out.println("entrou rdbuff1");
                    //if(posicao[2].equals(null)){
                    String posi = "F1,BUFFER,LENGTH";
                    //System.out.println("cfdgth: "+posi);
                    substituiRDBUFF(i, posi);

                }
                if (posicao[1].equals("WRBUFF"))
                {
                    //System.out.println("entrou wrbuff");
                    substituiWRBUFF(i, posicao[2]);
                }
                else
                {
                    //System.out.println("Não é nenhum");
                }
                //------Aqui fim
                k = expandirMacro(posicao); //Cuidar quando expande a macro, tem que verificar se tem chamada na macro que foi expandida anteriormente
            }
        }
        String[] arrayAux = new String[fim.size()];
        fim.toArray(arrayAux);
        arrayFinal = arrayAux;
    }

    /**
     * Percorre o arquivo procurando as chamadas de macro, onde encontrar
     * expande.
     *
     * @param posicao É um array onde cada posição é uma palavra da linha
     * @return Retorna o número de linhas expandido, para conseguir ver se tem
     * macro dentro de macro
     */
    public int expandirMacro(String[] posicao)
    {
        int i = 0, tamanho = 0, k = 0;
        if (posicao[1].equals("RDBUFF"))
        {
            tamanho = tamanhoRDBUFF;
            for (i = 1; i < tamanho - 1; i++)
            {
                //System.out.println("RDBUFF: "+arrayRDBUFF[i]);
                if ((i == 1) && (!label.equals(" ")))
                {
                    fim.add(label.concat(arraySubsRDBUFF[i]));
                }
                else
                {
                    fim.add(arraySubsRDBUFF[i]);
                }
                k = k + 1;
            }
        }
        if (posicao[1].equals("WRBUFF"))
        {
            //System.out.print("Tamanho: "+tamanhoWRBUFF);
            tamanho = tamanhoWRBUFF;
            for (i = 1; i < tamanho - 1; i++)
            {
                //System.out.println("WRDBUFF: "+arrayWRBUFF[i]);
                if ((i == 1) && (!label.equals(" ")))
                {
                    fim.add(label.concat(arraySubsWRBUFF[i]));
                }
                else
                {
                    fim.add(arraySubsWRBUFF[i]);
                }
                k = k + 1;
            }
        }
        return k;
    }

    public void substituiWRBUFF(int linha, String posicao)
    {
        arraySubsWRBUFF = arrayWRBUFF;
        int i = 0;
        String[] cabecalho = arrayWRBUFF[0].split("\t");
        String[] onde = cabecalho[2].split(",");
        String pos1, pos2, pos3, ondeTroca1, ondeTroca2, ondeTroca3;
        //System.out.println("Onde[0]: "+onde[0]);
        //System.out.println("Onde[1]: "+onde[1]);
        //System.out.println("Onde[2]: "+onde[2]);
        //if(!cabecalho[2].equals(" ")){
        String[] palavra = posicao.split(",");
        pos1 = palavra[0]; //05
        pos2 = palavra[1]; //buffer/eof
        pos3 = palavra[2]; //length/three
        ondeTroca1 = onde[0]; //&outdev
        ondeTroca2 = onde[1]; //&bufadr
        ondeTroca3 = onde[2]; //&reclth

        //System.out.println("Pos1: "+pos1);
        //System.out.println("Pos2: "+pos2);
        //System.out.println("Pos3: "+pos3);
        for (i = 0; i < tamanhoWRBUFF; i++)
        {
            if (arraySubsWRBUFF[i].contains(ondeTroca1))
            {
                arraySubsWRBUFF[i] = arraySubsWRBUFF[i].replace(ondeTroca1, pos1);
            }
            if (arraySubsWRBUFF[i].contains(ondeTroca2))
            {
                arraySubsWRBUFF[i] = arraySubsWRBUFF[i].replace(ondeTroca2, pos2);
            }
            if (arraySubsWRBUFF[i].contains(ondeTroca3))
            {
                arraySubsWRBUFF[i] = arraySubsWRBUFF[i].replace(ondeTroca3, pos3);
            }
            else
            {
                //System.out.println("Não tem pra trocar");
            }
            //System.out.println(arraySubsWRBUFF[i]);
        }
    }

    public void substituiRDBUFF(int linha, String posicao)
    {
        arraySubsRDBUFF = arrayRDBUFF;
        int i = 0;
        //System.out.println("entrou rdbufffffff");
        String[] cabecalho = arrayRDBUFF[0].split("\t");
        String[] onde = cabecalho[2].split(",");
        String pos1, pos2, pos3, ondeTroca1, ondeTroca2, ondeTroca3;
        //System.out.println("Onde[0]: "+onde[0]);
        //System.out.println("Onde[1]: "+onde[1]);
        //System.out.println("Onde[2]: "+onde[2]);
        //if(!cabecalho[2].equals(" ")){
        String[] palavra = posicao.split(",");
        pos1 = palavra[0]; //05
        pos2 = palavra[1]; //buffer/eof
        pos3 = palavra[2]; //length/three
        ondeTroca1 = onde[0]; //&outdev
        ondeTroca2 = onde[1]; //&bufadr
        ondeTroca3 = onde[2]; //&reclth

        //System.out.println("Pos1: "+pos1);
        //System.out.println("Pos2: "+pos2);
        //System.out.println("Pos3: "+pos3);
        for (i = 0; i < tamanhoRDBUFF; i++)
        {
            if (arraySubsRDBUFF[i].contains(ondeTroca1))
            {
                arraySubsRDBUFF[i] = arraySubsRDBUFF[i].replace(ondeTroca1, pos1);
            }
            if (arraySubsRDBUFF[i].contains(ondeTroca2))
            {
                arraySubsRDBUFF[i] = arraySubsRDBUFF[i].replace(ondeTroca2, pos2);
            }
            if (arraySubsRDBUFF[i].contains(ondeTroca3))
            {
                arraySubsRDBUFF[i] = arraySubsRDBUFF[i].replace(ondeTroca3, pos3);
            }
            else
            {
                //System.out.println("Não tem pra trocar");
            }
            //System.out.println(arraySubsWRBUFF[i]);
        }
    }

    public String expandMacros(String code)
    {
        // contém mais o menos o que era o main() de "Macro.class"
        // main() tinha "throws IOException"

        //Macro macro = new Macro();
        try
        {
            this.leEntrada(code); // modificado pra receber código em String
        }
        catch (IOException e)
        {
            Logger.getLogger(MacroExpander.class.getName()).log(Level.WARNING, null, e);
        }
        //this.leEntrada("entrada.txt");
        this.separaChamada();
        this.procuraChamadaDeMacro();
        //System.out.println("---------Array Final--------------");
        //this.imprimeArrayFinal();
        return this.imprimeArrayFinal(); // modificado pra retornar todo "ArrayFinal" em String
    }
}
