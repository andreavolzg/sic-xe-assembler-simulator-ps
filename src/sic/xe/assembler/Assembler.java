package sic.xe.assembler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import sic.xe.SICXE;

public class Assembler
{
    private Code code; // Instancia da classe Code, que armazena o código
    private int addressLocation, startingAddress;
    private SymbolTable table; // tabela de símbolos
    private Queue<Integer> machineCode; // fila de inteiros que armazena o código de máquina gerado
    private List<Instruction> instructions;

    /**
     * Recebe como parâmetro o código inteiro como string para transformar na classe code
     *
     * @param entireCode Código inteiro como uma String
     */
    public Assembler(String entireCode)
    {
        if (entireCode.startsWith("*"))
        {
            setStartingAddress(entireCode);
        }
        else
        {
            startingAddress = 0;
        }
        code = new Code(entireCode);
        table = new SymbolTable();
        addressLocation = 0;
        machineCode = new LinkedList<>();
        instructions = new ArrayList<>();
    }

    /**
     * Inicializa as variáveis necessárias
     */
    public Assembler()
    {
        table = new SymbolTable();
        addressLocation = 0;
        machineCode = new LinkedList<>();
        instructions = new ArrayList<>();
    }

    /**
     * Monta o código.
     */
    public void assemble()
    {
        firstPass();
        secondPass();
    }

    /**
     * Recebe como parâmetro uma String contendo o código inteiro para usar na classe Code
     *
     * @param entireCode Código inteiro como uma string
     */
    public void setCode(String entireCode)
    {
        if (entireCode.startsWith("*"))
        {
            setStartingAddress(entireCode);
        }
        else
        {
            startingAddress = 0;
        }
        code = new Code(entireCode);
    }

    /**
     * Imprime na tela o código de máquina gerado
     */
    public void printMC()
    {
        machineCode.stream().forEach((i) ->
        {
            System.out.print(i + " ");
        });
        System.out.print("\n");
    }

    public void printInstructions()
    {
        instructions.forEach((i) ->
        {
            System.out.println("[" + i.getAddress() + "] " + i.getFullInstruction());
        });
        System.out.print("\n");
    }

    public void loadToMemory()
    {
        instructions.forEach((piece) ->
        {
            SICXE.machine.setMemoryWordAt(piece.getFullInstruction(), piece.getAddress());
        });
    }

    /**
     * imprime na tela a tabela de símbolos
     */
    public void printST()
    {
        table.printTable();
    }

    private void setStartingAddress(String code)
    {
        String aux = "";
        int i = 1;
        while (code.charAt(i) != ' ')
        {
            aux += code.charAt(i++);
        }
        startingAddress = Integer.parseInt(aux);
    }

    /**
     * Retorna o endereço onde o código deve iniciar na memória
     * Que deverá ser o valor do PC ao início da execução.
     */
    public int getStartingAddress()
    {
        return startingAddress;
    }

    /**
     * Primeira passada do montador. Pega todos tokens do código e monta a tabela de símbolos.
     */
    private void firstPass()
    {
        String aux;
        aux = code.getNextToken();  /// start of a new line
        while (aux != null)
        {
            if (!isOperation(aux)) // is label
            {
                table.addLine(aux, addressLocation);
                aux = code.getNextToken(); // aux is now a operand
            }
            if (numberOfOperands(aux) != 0)
            {
                table.addLine(code.getNextToken(), -1);
                addressLocation++;
                if (numberOfOperands(aux) == 2)
                {
                    table.addLine(code.getNextToken(), -1);
                    addressLocation++;
                }
            }
            aux = code.getNextToken();  /// start of a new line
            addressLocation++;
        }
    }

    /**
     * Segundo passo do montador. Gera o código de máquina a partir do código e da tabela de símbolos.
     * Não usar esse método, e sim o método "secondPass()"
     */
    private void secondPassMC()
    {
        String aux;
        code.resetCounter();
        aux = code.getNextToken();  /// start of a new line
        while (aux != null)
        {
            if (isOperation(aux)) // if aux isnt a operation, its a label, thus not going to machine code
            {
                machineCode.add(getOPCode(aux));
                if (numberOfOperands(aux) == 1)
                {
                    machineCode.add(table.getSymbolAddress(code.getNextToken()));
                }
                else if (numberOfOperands(aux) == 2)
                {
                    machineCode.add(table.getSymbolAddress(code.getNextToken()));
                    machineCode.add(table.getSymbolAddress(code.getNextToken()));
                }
            }
            aux = code.getNextToken();
        }
    }

    private void secondPass()
    {
        String aux;
        code.resetCounter();
        aux = code.getNextToken();  /// start of a new line
        addressLocation = startingAddress;
        while (aux != null)
        {
            if (isOperation(aux)) // if aux isnt a operation, its a label or space, thus not going to machine code
            {
                if (format(aux) == 3)
                {
                    newB3Instruction(addressLocation, aux, code.getNextToken());
                }
                else if (format(aux) == 2)
                {
                   instructions.add(new B2Instruction(addressLocation, getOPCode(aux), Integer.parseInt(code.getNextToken()), Integer.parseInt(code.getNextToken())));
                }
                else if (format(aux) == 1)
                {
                    instructions.add(new B1Instruction(addressLocation, getOPCode(aux)));
                }
                addressLocation += 3;
            }
            aux = code.getNextToken();
        }
    }

    private void newB3Instruction(int address, String aux, String m)
    {
        if (m.startsWith("#"))
        { // is immediate
            instructions.add(new B3Instruction(address, getOPCode(aux), 0, 1, 0, 0, 0, getValue(m)));
        }
        else
        {
            instructions.add(new B3Instruction(address, getOPCode(aux), 1, 0, 0, 0, 0, getValue(m)));
        }
    }

    private int getValue(String parameter)
    {
        if (isImediate(parameter))
        {
            return getImediateValue(parameter);
        }
        else
        {
            return table.getSymbolAddress(parameter);
        }
    }

    /**
     * testa se um operador é imediato ou não
     *
     * @param aux operador
     * @return true se for operador, false c.c.
     */
    private boolean isImediate(String aux)
    {
        if (aux.startsWith("#"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private int getImediateValue(String aux)
    {
        return Integer.parseInt(aux.substring(1));
    }

    /**
     * Retorna o número de operandos de uma String, que é um token do código
     *
     * @param operation token que é um operador (ADD, LDA, etc)
     * @return Número de operandos ("parâmetros") que esse operador precisa
     */
    private int numberOfOperands(String operation)
    {
        if (operation.equals("FIX") || operation.equals("FLOAT") || operation.equals("HIO") || operation.equals("NORM")
            || operation.equals("RSUB") || operation.equals("SIO") || operation.equals("TIO") || operation.equals("SPACE"))
        {
            return 0;
        }
        else if (operation.equals("ADDR") || operation.equals("COMPR") || operation.equals("DIVR") || operation.equals("MULR")
                 || operation.equals("RMO") || operation.equals("SHIFTL") || operation.equals("SHIFTL") || operation.equals("SUBR"))
        {
            return 2;
        }
        else
        {
            return 1;
        }
    }

    /**
     * Retorna se uma string é operador ou não (ADD, LDA, etc)
     *
     * @param token string que vai ser avaliada
     * @return true se for operador ou false c.c.
     */
    private boolean isOperation(String token)
    {
        if (token.equals("ADD") || token.equals("LDA") || token.equals("ADDR") || token.equals("AND") || token.equals("CLEAR")
            || token.equals("COMP") || token.equals("COMPR") || token.equals("DIV") || token.equals("DIVR") || token.equals("J") || token.equals("JEQ")
            || token.equals("JGT") || token.equals("JLT") || token.equals("JSUB") || token.equals("LDB") || token.equals("LDCH") || token.equals("LDL")
            || token.equals("LDS") || token.equals("LDT") || token.equals("LDX") || token.equals("MUL") || token.equals("MULR") || token.equals("OR")
            || token.equals("RD") || token.equals("RMO") || token.equals("RSUB") || token.equals("SHIFTL") || token.equals("SHIFTR") || token.equals("STA")
            || token.equals("STB") || token.equals("STCH") || token.equals("STL") || token.equals("STS") || token.equals("STT") || token.equals("STX")
            || token.equals("SUB") || token.equals("SUBR") || token.equals("TD") || token.equals("TIX") || token.equals("TXR") || token.equals("WD"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Retorna o formato da instrução, para saber qual tipo de instrução usar
     *
     * @param aux Mnemonic
     * @return formato correspondente da isntrução
     */
    private int format(String aux)
    {
        if((aux.endsWith("R") && !aux.equals("CLEAR")) || aux.equals("SHIFTL") || aux.equals("SVC"))
        {
            return 2;
        }
        else if (aux.equals("SIO") || aux.equals("TIO") || aux.equals("HIO"))
        {
            return 1;
        }
        else
        {
            return 3;
        }
    }

    /**
     * Retorna o OP Code de um operador (ADD, LDA, etc)
     *
     * @param operation Operador
     * @return seu OP Code
     */
    private int getOPCode(String operation)
    {
        if (operation.equals("ADD"))
        {
            return 6;
        }
        else if (operation.equals("ADDR"))
        {
            return 36;
        }
        else if (operation.equals("AND"))
        {
            return 16;
        }
        else if (operation.equals("CLEAR"))
        {
            return 45;
        }
        else if (operation.equals("COMP"))
        {
            return 10;
        }
        else if (operation.equals("COMPR"))
        {
            return 40;
        }
        else if (operation.equals("DIV"))
        {
            return 9;
        }
        else if (operation.equals("DIVR"))
        {
            return 39;
        }
        else if (operation.equals("J"))
        {
            return 15;
        }
        else if (operation.equals("JEQ"))
        {
            return 12;
        }
        else if (operation.equals("JGT"))
        {
            return 13;
        }
        else if (operation.equals("JLT"))
        {
            return 14;
        }
        else if (operation.equals("JSUB"))
        {
            return 18;
        }
        else if (operation.equals("LDA"))
        {
            return 0;
        }
        else if (operation.equals("LDB"))
        {
            return 26;
        }
        else if (operation.equals("LDCH"))
        {
            return 20;
        }
        else if (operation.equals("LDL"))
        {
            return 2;
        }
        else if (operation.equals("LDS"))
        {
            return 27;
        }
        else if (operation.equals("LDT"))
        {
            return 28;
        }
        else if (operation.equals("LDX"))
        {
            return 2;
        }
        else if (operation.equals("MUL"))
        {
            return 8;
        }
        else if (operation.equals("MULR"))
        {
            return 38;
        }
        else if (operation.equals("OR"))
        {
            return 17;
        }
        else if (operation.equals("RD"))
        {
            return 54;
        }
        else if (operation.equals("RMO"))
        {
            return 43;
        }
        else if (operation.equals("RSUB"))
        {
            return 19;
        }
        else if (operation.equals("SHIFTL"))
        {
            return 41;
        }
        else if (operation.equals("SHIFTR"))
        {
            return 42;
        }
        else if (operation.equals("STA"))
        {
            return 3;
        }
        else if (operation.equals("STB"))
        {
            return 30;
        }
        else if (operation.equals("STCH"))
        {
            return 84;
        }
        else if (operation.equals("STL"))
        {
            return 20;
        }
        else if (operation.equals("STS"))
        {
            return 31;
        }
        else if (operation.equals("STT"))
        {
            return 32;
        }
        else if (operation.equals("STX"))
        {
            return 16;
        }
        else if (operation.equals("SUB"))
        {
            return 7;
        }
        else if (operation.equals("SUBR"))
        {
            return 37;
        }
        else if (operation.equals("TD"))
        {
            return 56;
        }
        else if (operation.equals("TIX"))
        {
            return 11;
        }
        else if (operation.equals("WD"))
        {
            return 55;
        }
        else
        {
            return 0;
        }
    }

}
