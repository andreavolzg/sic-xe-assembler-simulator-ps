package sic.xe.assembler;

public abstract class Instruction
{
    protected int opcode;
    protected int address;

    public Instruction(int address, int opcode)
    {
        this.address = address;
        this.opcode = opcode;
    }

    public abstract int getFullInstruction();

    public int getAddress()
    {
        return address;
    }
}
