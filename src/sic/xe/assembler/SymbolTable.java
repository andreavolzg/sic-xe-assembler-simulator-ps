package sic.xe.assembler;

import java.util.ArrayList;
import java.util.List;

public class SymbolTable
{
    public class TableLine
    {

        private final String symbol;
        private int address;

        public TableLine(String symbol, int address)
        {
            this.symbol = symbol;
            this.address = address;
        }

        public String getSymbol()
        {
            return symbol;
        }

        public int getAddress()
        {
            return address;
        }

        void setAddress(int address)
        {
            this.address = address;
        }

    }

    private List<TableLine> lines; // a tabela de símbolos é uma lista da classe TableLine

    /**
     * Inicializa a lista como um array
     */
    public SymbolTable()
    {
        lines = new ArrayList<>();
    }

    // Public methods
    /**
     * Adiciona uma linha na tabela.
     *
     * @param symbol símbolo desta linha
     * @param address endereço que o símbolo está no código, ou -1 caso ainda seja indefinido
     */
    public void addLine(String symbol, int address)
    {
        if (existInSymbolTable(symbol))
        {
            if (address != -1 && isUndefined(symbol))
            {
                updateLine(symbol, address);
            }
        }
        else if (!symbol.startsWith("#"))
        {
            lines.add(new TableLine(symbol, address));
        }
    }

    /**
     * Retorna se um símbolo contido na tabela é definido ou não
     *
     * @param symbol símbolo que será testado
     * @return retorna true se for indefinido, false c.c.
     */
    public boolean isUndefined(String symbol)
    {
        for (TableLine aux : lines)
        {
            if (aux.getSymbol().equals(symbol) && aux.getAddress() == -1)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna o endereço de um símbolo contido na tabela
     *
     * @param symbol símbolo da tabela
     * @return endereço do símbolo
     */
    public int getSymbolAddress(String symbol)
    {
        for (TableLine aux : lines)
        {
            if (aux.getSymbol().equals(symbol))
            {
                return aux.getAddress();
            }
        }
        return -1;
    }

    /**
     * Imprime na tela a tabela de símbolos
     */
    public void printTable()
    {
        lines.stream().forEach((i) ->
        {
            System.out.println("Symbol: " + i.getSymbol() + " Address: " + i.getAddress());
        });
    }

    // Private methods
    /**
     * Atualiza linha da tabela. Por enquanto ela só atualiza o endereço.
     *
     * @param symbol Símbolo da tabela que será atualizado
     * @param address novo endereço deste símbolo
     */
    private void updateLine(String symbol, int address)
    {
        TableLine aux;
        for (int i = 0; i < lines.size(); i++) // runs through the table searching for the symbol
        {
            aux = lines.get(i);
            if (aux.getSymbol().equals(symbol)) // if we find the symbol
            {
                aux.setAddress(address); // update the address
                lines.set(i, aux); // returns it to the array
                return; // break
            }
        }
    }

    /**
     * Testa se um símbolo já existe na tabela
     *
     * @param symbol símbolo a ser testado
     * @return true se existir, falso c.c.
     */
    private boolean existInSymbolTable(String symbol)
    {
        for (TableLine aux : lines)
        {
            if (aux.getSymbol().equals(symbol))
            {
                return true;
            }
        }
        return false;
    }

}
