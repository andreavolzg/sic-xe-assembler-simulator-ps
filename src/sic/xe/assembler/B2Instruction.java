package sic.xe.assembler;

public class B2Instruction extends Instruction
{
    private final int r1, r2;

    public B2Instruction(int address, int opcode, int r1, int r2)
    {
        super(address, opcode);
        this.r1 = r1;
        this.r2 = r2;
    }

    @Override
    public int getFullInstruction()
    {
        int inst = 0;
        inst = inst | (opcode << 18);
        inst = inst | (r1 << 12);
        inst = inst | (r2 << 8);
        return inst;
    }
}
