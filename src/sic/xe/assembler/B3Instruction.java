package sic.xe.assembler;

public class B3Instruction extends Instruction
{
    private final int n, i, x, b, p, displacement; // como a flag 'e' era sempre 0, nem uso ela

    public B3Instruction(int address, int opcode, int n, int i, int x, int b, int p, int displacement)
    {
        super(address, opcode);
        this.n = n;
        this.i = i;
        this.x = x;
        this.b = b;
        this.p = p;
        //this.e = e;
        this.displacement = displacement;
    }

    @Override
    public int getFullInstruction()
    {
        int inst = 0;
        inst = inst | (opcode << 18);
        inst = inst | (n << 17);
        inst = inst | (i << 16);
        inst = inst | (x << 15);
        inst = inst | (b << 14);
        inst = inst | (p << 13);
        //inst = inst&(e<<12);
        inst = inst | displacement;

        return inst;
    }
}
