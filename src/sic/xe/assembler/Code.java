package sic.xe.assembler;

import java.util.ArrayList;
import java.util.List;

public class Code
{
    private final List<String> tokens;
    private int counter; // contador interno

    /**
     * Construtor que inicializa variáveis e recebe como parâmetro o código inteiro como uma string.
     *
     * @param entireCode Código completo como uma string
     */
    public Code(String entireCode)
    {
        tokens = new ArrayList<>();
        stringToTokens(entireCode);
        counter = -1; // in the first call of "getNextToken" counter will be incremented before used
    }

    // public methods
    /**
     * Retorna o próximo token, atualizando o contador.
     *
     * @return próximo token do código, de acordo com o contador interno
     */
    public String getNextToken()
    {
        counter++;
        if (counter < tokens.size()) // avoiding a null pointer exception
        {
            return tokens.get(counter);
        }
        else
        {
            return null;
        }
    }

    /**
     * Reseta o contador, caso desejarmos voltar para o começo do código
     */
    public void resetCounter()
    {
        this.counter = -1;
    }

    /**
     * Retorna o contador
     *
     * @return contador
     */
    public int getCounter()
    {
        return counter;
    }

    /// Private methods
    /**
     * recebe o código como uma string e converte pra o array de tokens
     *
     * @param entireCode Código inteiro como uma única string
     */
    private void stringToTokens(String entireCode)
    {
        int i = 0;
        while (i < entireCode.length())
        {
            i = searchNextToken(i, entireCode); // procura pelo próximo token, partindo de i
        }
    }

    /**
     * Procura pelo próximo token dentro do código, partindo da posição i, passando por espaços em branco
     * e \n's
     *
     * @param i posição inicial do código
     * @param entireCode Código inteiro como uma única string
     * @return novo i para a próxima iteração do método stringToTokens
     */
    private int searchNextToken(int i, String entireCode)
    {
        while (entireCode.charAt(i) == '.') // ignora comentários
        {
            if (entireCode.charAt(i) == '.')
            {
                while (entireCode.charAt(i) != '\n')
                {
                    i++;
                }
                i++;
            }
        }

        // procura pelo próximo token
        while (i < entireCode.length() && (entireCode.charAt(i) == ' ' || entireCode.charAt(i) == '\n'))
        {
            i++;
        }
        return addToken(i, entireCode);
    }

    /**
     * Adiciona um token encontrado no código para a lista de tokens
     *
     * @param i posição inicial do código
     * @param entireCode Código inteiro como uma única string
     * @return a posição final do token na string
     */
    private int addToken(int i, String entireCode)
    {
        String aux = "";
        while (i < entireCode.length() && (entireCode.charAt(i) != ' ' && entireCode.charAt(i) != '\n'))
        {
            aux = aux + entireCode.charAt(i);
            i++;
        }
        if (!aux.equals("")) // we can have some blank spaces after the last token
        {
            tokens.add(aux);
        }
        return i;
    }
}
