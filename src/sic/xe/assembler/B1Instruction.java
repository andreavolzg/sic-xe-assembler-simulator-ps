package sic.xe.assembler;

public class B1Instruction extends Instruction
{
    public B1Instruction(int address, int opcode)
    {
        super(address, opcode);
    }

    @Override
    public int getFullInstruction()
    {
        int inst = 0;
        return inst | (opcode << 16);
    }
}
