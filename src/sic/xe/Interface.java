package sic.xe;

import java.awt.Color;
import java.io.*;
import java.util.logging.*;
import javax.swing.*;
import sic.xe.assembler.Assembler;

public class Interface extends javax.swing.JFrame
{
    private short registers_nbase;
    private short memory_nbase;
    private String blank_word;
    private String blank_byte;

    private int memory_first_pos;
    private int memory_last_pos;

    private String undoBuffer;

    private JFileChooser fileChooser;

    private javax.swing.JMenuBar MenuBar;
    private javax.swing.JMenu MenuBar_File;
    private javax.swing.JMenuItem MenuBar_File_Open;
    private javax.swing.JMenuItem MenuBar_File_Save;
    private javax.swing.JPopupMenu.Separator MenuBar_File__a;
    private javax.swing.JMenuItem MenuBar_File_Restart;
    private javax.swing.JMenuItem MenuBar_File_Quit;
    private javax.swing.JPanel IO_AREA;
    private javax.swing.JScrollPane MAIN_INPUT_sp;
    private javax.swing.JTextArea MAIN_INPUT;
    private javax.swing.JPanel MAIN_ACTIONS;
    private javax.swing.JButton BUTTON_Expand_Macros;
    private javax.swing.JButton BUTTON_Assemble;
    private javax.swing.JButton BUTTON_Run;
    private javax.swing.JButton BUTTON_Undo;
    private javax.swing.JScrollPane MAIN_OUTPUT_sp;
    private javax.swing.JTextArea MAIN_OUTPUT;
    private javax.swing.JPanel REGISTERS_AREA;
    private javax.swing.JLabel title_REGISTERS;
    private javax.swing.JLabel register_label_A;
    private javax.swing.JTextField register_content_A;
    private javax.swing.JLabel register_label_X;
    private javax.swing.JTextField register_content_X;
    private javax.swing.JLabel register_label_L;
    private javax.swing.JTextField register_content_L;
    private javax.swing.JLabel register_label_B;
    private javax.swing.JTextField register_content_B;
    private javax.swing.JLabel register_label_S;
    private javax.swing.JTextField register_content_S;
    private javax.swing.JLabel register_label_T;
    private javax.swing.JTextField register_content_T;
    private javax.swing.JLabel register_label_F;
    private javax.swing.JTextField register_content_F;
    private javax.swing.JLabel register_label_PC;
    private javax.swing.JTextField register_content_PC;
    private javax.swing.JLabel register_label_SW;
    private javax.swing.JTextField register_content_SW;
    private javax.swing.ButtonGroup registers_nbase_options;
    private javax.swing.JRadioButton registers_nbase_BIN;
    private javax.swing.JRadioButton registers_nbase_DEC;
    private javax.swing.JRadioButton registers_nbase_HEX;
    private javax.swing.JPanel MEMORY_AREA;
    private javax.swing.JLabel title_MEMORY;
    private javax.swing.JScrollPane MAIN_MEMORY_sp;
    private javax.swing.JTable MAIN_MEMORY;
    private javax.swing.table.DefaultTableCellRenderer cellRenderer;
    private javax.swing.JLabel memory_range_text;
    private javax.swing.JTextField memory_range_first;
    private javax.swing.ButtonGroup memory_nbase_options;
    private javax.swing.JRadioButton memory_nbase_BIN;
    private javax.swing.JRadioButton memory_nbase_DEC;
    private javax.swing.JRadioButton memory_nbase_HEX;

    private void initializeFileChooser()
    {
        this.fileChooser = new JFileChooser();
        this.fileChooser.setFileHidingEnabled(false);
        this.fileChooser.setMultiSelectionEnabled(false);
        this.fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    private void setLookAndFeel()
    {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException eA)
        {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, eA);
        }
        catch (javax.swing.UnsupportedLookAndFeelException e)
        {
            try
            {
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            }
            catch (ClassNotFoundException | InstantiationException |
                   IllegalAccessException | UnsupportedLookAndFeelException eB)
            {
                Logger.getLogger(SICXE.class.getName()).log(Level.SEVERE, null, eB);
            }
        }
    }

    private void initializeGUIElements()
    {
        MenuBar = new javax.swing.JMenuBar();
        MenuBar_File = new javax.swing.JMenu();
        MenuBar_File_Open = new javax.swing.JMenuItem();
        MenuBar_File_Save = new javax.swing.JMenuItem();
        MenuBar_File__a = new javax.swing.JPopupMenu.Separator();
        MenuBar_File_Restart = new javax.swing.JMenuItem();
        MenuBar_File_Quit = new javax.swing.JMenuItem();
        IO_AREA = new javax.swing.JPanel();
        MAIN_INPUT_sp = new javax.swing.JScrollPane();
        MAIN_INPUT = new javax.swing.JTextArea();
        MAIN_ACTIONS = new javax.swing.JPanel();
        BUTTON_Undo = new javax.swing.JButton();
        BUTTON_Expand_Macros = new javax.swing.JButton();
        BUTTON_Assemble = new javax.swing.JButton();
        BUTTON_Run = new javax.swing.JButton();
        MAIN_OUTPUT_sp = new javax.swing.JScrollPane();
        MAIN_OUTPUT = new javax.swing.JTextArea();
        REGISTERS_AREA = new javax.swing.JPanel();
        title_REGISTERS = new javax.swing.JLabel();
        register_label_A = new javax.swing.JLabel();
        register_content_A = new javax.swing.JTextField();
        register_label_X = new javax.swing.JLabel();
        register_content_X = new javax.swing.JTextField();
        register_label_L = new javax.swing.JLabel();
        register_content_L = new javax.swing.JTextField();
        register_label_B = new javax.swing.JLabel();
        register_content_B = new javax.swing.JTextField();
        register_label_S = new javax.swing.JLabel();
        register_content_S = new javax.swing.JTextField();
        register_label_T = new javax.swing.JLabel();
        register_content_T = new javax.swing.JTextField();
        register_label_F = new javax.swing.JLabel();
        register_content_F = new javax.swing.JTextField();
        register_label_PC = new javax.swing.JLabel();
        register_content_PC = new javax.swing.JTextField();
        register_label_SW = new javax.swing.JLabel();
        register_content_SW = new javax.swing.JTextField();
        registers_nbase_options = new javax.swing.ButtonGroup();
        registers_nbase_BIN = new javax.swing.JRadioButton();
        registers_nbase_HEX = new javax.swing.JRadioButton();
        registers_nbase_DEC = new javax.swing.JRadioButton();
        MEMORY_AREA = new javax.swing.JPanel();
        title_MEMORY = new javax.swing.JLabel();
        MAIN_MEMORY_sp = new javax.swing.JScrollPane();
        MAIN_MEMORY = new javax.swing.JTable();
        cellRenderer = new javax.swing.table.DefaultTableCellRenderer();
        memory_range_text = new javax.swing.JLabel();
        memory_range_first = new javax.swing.JTextField();
        memory_nbase_options = new javax.swing.ButtonGroup();
        memory_nbase_BIN = new javax.swing.JRadioButton();
        memory_nbase_HEX = new javax.swing.JRadioButton();
        memory_nbase_DEC = new javax.swing.JRadioButton();
    }

    private void buildMenuBar()
    {
        MenuBar.setMinimumSize(new java.awt.Dimension(71, 21));
        MenuBar.setName("Menu Bar");
        MenuBar.setNextFocusableComponent(MAIN_INPUT);

        MenuBar_File.setText("File");
        MenuBar_File.setFont(MenuBar_File.getFont());
        MenuBar_File.setNextFocusableComponent(MAIN_INPUT);

        MenuBar_File_Open.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        MenuBar_File_Open.setFont(MenuBar_File_Open.getFont());
        MenuBar_File_Open.setText("  Open                 ");
        MenuBar_File_Open.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            open();
        });
        MenuBar_File.add(MenuBar_File_Open);

        MenuBar_File_Save.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        MenuBar_File_Save.setFont(MenuBar_File_Save.getFont());
        MenuBar_File_Save.setText("  Save                  ");
        MenuBar_File_Save.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            save();
        });
        MenuBar_File.add(MenuBar_File_Save);
        MenuBar_File.add(MenuBar_File__a);

        MenuBar_File_Restart.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        MenuBar_File_Restart.setFont(MenuBar_File_Restart.getFont());
        MenuBar_File_Restart.setText("  Restart                 ");
        MenuBar_File_Restart.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            restart();
        });
        MenuBar_File.add(MenuBar_File_Restart);

        MenuBar_File_Quit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        MenuBar_File_Quit.setFont(MenuBar_File_Quit.getFont());
        MenuBar_File_Quit.setText("  Quit                  ");
        MenuBar_File_Quit.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            quit();
        });
        MenuBar_File.add(MenuBar_File_Quit);

        MenuBar.add(MenuBar_File);

        setJMenuBar(MenuBar);
    }

    private void buildIOArea()
    {
        MAIN_INPUT.setColumns(20);
        MAIN_INPUT.setRows(5);
        MAIN_INPUT.setTabSize(4);
        MAIN_INPUT.setNextFocusableComponent(BUTTON_Undo);
        MAIN_INPUT_sp.setViewportView(MAIN_INPUT);

        BUTTON_Undo.setToolTipText("Expand macros on the input code");
        BUTTON_Undo.setText("UNDO");
        BUTTON_Expand_Macros.setNextFocusableComponent(BUTTON_Expand_Macros);
        BUTTON_Undo.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            undo();
        });
        BUTTON_Undo.setEnabled(false);

        BUTTON_Expand_Macros.setToolTipText("Expand macros on the input code");
        BUTTON_Expand_Macros.setLabel("EXPAND MACROS");
        BUTTON_Expand_Macros.setNextFocusableComponent(BUTTON_Assemble);
        BUTTON_Expand_Macros.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            expandMacrosOnInput();
        });

        BUTTON_Assemble.setToolTipText("Assemble and load the input code");
        BUTTON_Assemble.setLabel("ASSEMBLE/LOAD");
        BUTTON_Assemble.setNextFocusableComponent(BUTTON_Run);
        BUTTON_Assemble.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            assembleInput();
        });

        BUTTON_Run.setToolTipText("Run what is on the memory position pointed by PC (register)");
        BUTTON_Run.setLabel("RUN");
        BUTTON_Run.setNextFocusableComponent(MAIN_OUTPUT);
        BUTTON_Run.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            runCode();
        });

        if ((System.getProperty("os.name").toLowerCase().contains("win")))
        {
            BUTTON_Undo.setFont(BUTTON_Undo.getFont().deriveFont(BUTTON_Undo.getFont().getSize() + 2f));
            BUTTON_Expand_Macros.setFont(BUTTON_Expand_Macros.getFont().deriveFont(BUTTON_Expand_Macros.getFont().getSize() + 2f));
            BUTTON_Assemble.setFont(BUTTON_Assemble.getFont().deriveFont(BUTTON_Assemble.getFont().getSize() + 2f));
            BUTTON_Run.setFont(BUTTON_Run.getFont().deriveFont(BUTTON_Run.getFont().getSize() + 2f));
        }
        else
        {
            BUTTON_Undo.setFont(BUTTON_Undo.getFont().deriveFont(BUTTON_Undo.getFont().getSize() - 1f));
            BUTTON_Expand_Macros.setFont(BUTTON_Expand_Macros.getFont().deriveFont(BUTTON_Expand_Macros.getFont().getSize() - 1f));
            BUTTON_Assemble.setFont(BUTTON_Assemble.getFont().deriveFont(BUTTON_Assemble.getFont().getSize() - 1f));
            BUTTON_Run.setFont(BUTTON_Run.getFont().deriveFont(BUTTON_Run.getFont().getSize() - 1f));
        }

        javax.swing.GroupLayout MAIN_ACTIONSLayout = new javax.swing.GroupLayout(MAIN_ACTIONS);
        MAIN_ACTIONS.setLayout(MAIN_ACTIONSLayout);
        MAIN_ACTIONSLayout.setHorizontalGroup(
                MAIN_ACTIONSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(MAIN_ACTIONSLayout.createSequentialGroup()
                        .addGap(0, 0, 0)
                        .addComponent(BUTTON_Undo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BUTTON_Expand_Macros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BUTTON_Assemble, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BUTTON_Run, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        MAIN_ACTIONSLayout.setVerticalGroup(
                MAIN_ACTIONSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(MAIN_ACTIONSLayout.createSequentialGroup()
                        .addGap(0, 0, 0)
                        .addGroup(MAIN_ACTIONSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(BUTTON_Expand_Macros)
                                .addComponent(BUTTON_Assemble)
                                .addComponent(BUTTON_Run)
                                .addComponent(BUTTON_Undo))
                        .addGap(0, 0, 0))
        );

        MAIN_OUTPUT.setEditable(false);
        MAIN_OUTPUT.setBackground(new java.awt.Color(1, 1, 1));
        MAIN_OUTPUT.setColumns(20);
        MAIN_OUTPUT.setForeground(new java.awt.Color(195, 195, 195));
        MAIN_OUTPUT.setLineWrap(true);
        MAIN_OUTPUT.setRows(5);
        MAIN_OUTPUT.setTabSize(4);
        MAIN_OUTPUT.setNextFocusableComponent(register_content_A);
        MAIN_OUTPUT_sp.setViewportView(MAIN_OUTPUT);

        javax.swing.GroupLayout IO_AREALayout = new javax.swing.GroupLayout(IO_AREA);
        IO_AREA.setLayout(IO_AREALayout);
        IO_AREALayout.setHorizontalGroup(
                IO_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(IO_AREALayout.createSequentialGroup()
                        .addGap(0, 0, 0)
                        .addGroup(IO_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(MAIN_OUTPUT_sp)
                                .addComponent(MAIN_ACTIONS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(MAIN_INPUT_sp))
                        .addGap(0, 0, 0))
        );
        IO_AREALayout.setVerticalGroup(
                IO_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(IO_AREALayout.createSequentialGroup()
                        .addComponent(MAIN_INPUT_sp)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(MAIN_ACTIONS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(MAIN_OUTPUT_sp, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }

    private void buildRegistersArea()
    {
        title_REGISTERS.setFont(title_REGISTERS.getFont().deriveFont(title_REGISTERS.getFont().getStyle() | java.awt.Font.BOLD, title_REGISTERS.getFont().getSize() + 2));
        title_REGISTERS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        title_REGISTERS.setLabelFor(REGISTERS_AREA);
        title_REGISTERS.setText("REGISTERS");
        title_REGISTERS.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        title_REGISTERS.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        register_label_A.setFont(register_label_A.getFont().deriveFont(register_label_A.getFont().getStyle() | java.awt.Font.BOLD));
        register_label_A.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        register_label_A.setText("A");
        register_label_A.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        register_label_A.setName("register_label_A");

        register_content_A.setToolTipText("Accumulator; used for arithmetic operations.");
        register_content_A.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        register_content_A.setName("register_content_A");
        register_content_A.setNextFocusableComponent(register_content_X);

        register_label_X.setFont(register_label_X.getFont().deriveFont(register_label_X.getFont().getStyle() | java.awt.Font.BOLD));
        register_label_X.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        register_label_X.setText("X");
        register_label_X.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        register_label_X.setName("register_label_X");

        register_content_X.setToolTipText("Index register; used for addressing.");
        register_content_X.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        register_content_X.setName("register_content_X");
        register_content_X.setNextFocusableComponent(register_content_L);

        register_label_L.setFont(register_label_L.getFont().deriveFont(register_label_L.getFont().getStyle() | java.awt.Font.BOLD));
        register_label_L.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        register_label_L.setText("L");
        register_label_L.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        register_label_L.setName("register_label_L");

        register_content_L.setToolTipText("Linkage register; JSUB instruction stores the return address here.");
        register_content_L.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        register_content_L.setName("register_content_L");
        register_content_L.setNextFocusableComponent(register_content_B);

        register_label_B.setFont(register_label_B.getFont().deriveFont(register_label_B.getFont().getStyle() | java.awt.Font.BOLD));
        register_label_B.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        register_label_B.setText("B");
        register_label_B.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        register_label_B.setName("register_label_B");

        register_content_B.setToolTipText("Base register; used for addressing.");
        register_content_B.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        register_content_B.setName("register_content_B");
        register_content_B.setNextFocusableComponent(register_content_S);

        register_label_S.setFont(register_label_S.getFont().deriveFont(register_label_S.getFont().getStyle() | java.awt.Font.BOLD));
        register_label_S.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        register_label_S.setText("S");
        register_label_S.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        register_label_S.setName("register_label_S");

        register_content_S.setToolTipText("General working register (S).");
        register_content_S.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        register_content_S.setName("register_content_S");
        register_content_S.setNextFocusableComponent(register_content_T);

        register_label_T.setFont(register_label_T.getFont().deriveFont(register_label_T.getFont().getStyle() | java.awt.Font.BOLD));
        register_label_T.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        register_label_T.setText("T");
        register_label_T.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        register_label_T.setName("register_label_T");

        register_content_T.setToolTipText("General working register (T).");
        register_content_T.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        register_content_T.setName("register_content_T");
        register_content_T.setNextFocusableComponent(register_content_F);

        register_label_F.setFont(register_label_F.getFont().deriveFont(register_label_F.getFont().getStyle() | java.awt.Font.BOLD));
        register_label_F.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        register_label_F.setText("F");
        register_label_F.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        register_label_F.setName("register_label_F");

        register_content_F.setToolTipText("Floating-point accumulator; 48 bits long.");
        register_content_F.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        register_content_F.setName("register_content_F");
        register_content_F.setNextFocusableComponent(register_content_PC);

        register_label_PC.setFont(register_label_PC.getFont().deriveFont(register_label_PC.getFont().getStyle() | java.awt.Font.BOLD, register_label_PC.getFont().getSize() - 4));
        register_label_PC.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        register_label_PC.setText("PC");
        register_label_PC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        register_label_PC.setName("register_label_PC");

        register_content_PC.setToolTipText("Program Counter; contains the address of the next instruction to be executed.");
        register_content_PC.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        register_content_PC.setName("register_content_PC");
        register_content_PC.setNextFocusableComponent(register_content_SW);

        register_label_SW.setFont(register_label_SW.getFont().deriveFont(register_label_SW.getFont().getStyle() | java.awt.Font.BOLD, register_label_SW.getFont().getSize() - 4));
        register_label_SW.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        register_label_SW.setText("SW");
        register_label_SW.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        register_label_SW.setName("register_label_SW");

        register_content_SW.setToolTipText("Status word; contains various informations, including Condition Code (CC).");
        register_content_SW.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        register_content_SW.setName("register_content_SW");
        register_content_SW.setNextFocusableComponent(registers_nbase_BIN);

        registers_nbase_options.add(registers_nbase_BIN);
        registers_nbase_BIN.setFont(registers_nbase_BIN.getFont().deriveFont(registers_nbase_BIN.getFont().getSize() - 3f));
        registers_nbase_BIN.setText("BIN");
        registers_nbase_BIN.setName("registers_nbase_BIN");
        registers_nbase_BIN.setNextFocusableComponent(registers_nbase_DEC);
        registers_nbase_BIN.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            toggle_registers_nbase_BIN();
        });

        registers_nbase_options.add(registers_nbase_HEX);
        registers_nbase_HEX.setFont(registers_nbase_HEX.getFont().deriveFont(registers_nbase_HEX.getFont().getSize() - 3f));
        registers_nbase_HEX.setText("HEX");
        registers_nbase_HEX.setName("registers_nbase_HEX");
        registers_nbase_HEX.setNextFocusableComponent(MAIN_MEMORY);
        registers_nbase_HEX.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            toggle_registers_nbase_HEX();
        });

        registers_nbase_options.add(registers_nbase_DEC);
        registers_nbase_DEC.setFont(registers_nbase_DEC.getFont().deriveFont(registers_nbase_DEC.getFont().getSize() - 3f));
        registers_nbase_DEC.setText("DEC");
        registers_nbase_DEC.setName("registers_nbase_DEC");
        registers_nbase_DEC.setNextFocusableComponent(registers_nbase_HEX);
        registers_nbase_DEC.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            toggle_registers_nbase_DEC();
        });

        javax.swing.GroupLayout REGISTERS_AREALayout = new javax.swing.GroupLayout(REGISTERS_AREA);
        REGISTERS_AREA.setLayout(REGISTERS_AREALayout);
        REGISTERS_AREALayout.setHorizontalGroup(
                REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(register_label_A, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(register_content_A, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(register_label_X, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(register_content_X, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(register_label_L, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(register_content_L, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(register_label_B, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(register_content_B, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(register_label_S, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(register_content_S, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(register_label_T, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(register_content_T, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(register_label_F, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(register_content_F, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(register_label_SW, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(register_content_SW, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(register_label_PC, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(register_content_PC, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                .addComponent(title_REGISTERS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                        .addComponent(registers_nbase_BIN)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(registers_nbase_DEC)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(registers_nbase_HEX))
        );
        REGISTERS_AREALayout.setVerticalGroup(
                REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, REGISTERS_AREALayout.createSequentialGroup()
                          .addComponent(title_REGISTERS)
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                  .addGroup(REGISTERS_AREALayout.createSequentialGroup()
                                          .addComponent(register_label_A, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                  .addComponent(register_label_X, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                  .addComponent(register_content_X, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                  .addComponent(register_content_A, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                  .addComponent(register_label_L, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                  .addComponent(register_content_L, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                  .addComponent(register_label_B, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                  .addComponent(register_content_B, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                  .addComponent(register_label_S, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                  .addComponent(register_content_S, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                  .addComponent(register_label_T, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                  .addComponent(register_content_T, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                  .addComponent(register_label_F, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                  .addComponent(register_content_F, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                  .addComponent(register_label_PC, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                  .addComponent(register_content_PC, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                  .addComponent(register_label_SW, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                  .addComponent(register_content_SW, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                          .addGroup(REGISTERS_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                  .addComponent(registers_nbase_BIN, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                  .addComponent(registers_nbase_HEX, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                  .addComponent(registers_nbase_DEC, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                          .addGap(0, 0, 0))
        );
    }

    private void buildMemoryArea()
    {
        title_MEMORY.setFont(title_MEMORY.getFont().deriveFont(title_MEMORY.getFont().getStyle() | java.awt.Font.BOLD, title_MEMORY.getFont().getSize() + 2));
        title_MEMORY.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        title_MEMORY.setLabelFor(MAIN_MEMORY_sp);
        title_MEMORY.setText("MAIN MEMORY");
        title_MEMORY.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        title_MEMORY.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        MAIN_MEMORY_sp.setPreferredSize(new java.awt.Dimension(268, 602));

        MAIN_MEMORY.setModel(new javax.swing.table.DefaultTableModel(
                new String[1000][2], new String[]
                {
                    "address", "content"
                })
                {
                    Class[] types = new Class[]
                    {
                        java.lang.String.class, java.lang.String.class
                    };
                    boolean[] canEdit = new boolean[]
                    {
                        false, true
                    };

                    @Override
                    public Class getColumnClass(int columnIndex)
                    {
                        return types[columnIndex];
                    }

                    @Override
                    public boolean isCellEditable(int rowIndex, int columnIndex)
                    {
                        return canEdit[columnIndex];
                    }
                });

        MAIN_MEMORY.setColumnSelectionAllowed(true);
        MAIN_MEMORY.setNextFocusableComponent(memory_nbase_BIN);
        MAIN_MEMORY.getTableHeader().setReorderingAllowed(false);
        MAIN_MEMORY_sp.setViewportView(MAIN_MEMORY);
        MAIN_MEMORY.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        cellRenderer.setForeground(Color.GRAY);
        MAIN_MEMORY.getColumnModel().getColumn(0).setCellRenderer(cellRenderer);
        cellRenderer.setForeground(Color.BLACK);
        cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
        MAIN_MEMORY.getColumnModel().getColumn(1).setCellRenderer(cellRenderer);
        if (MAIN_MEMORY.getColumnModel().getColumnCount() > 0)
        {
            MAIN_MEMORY.getColumnModel().getColumn(0).setResizable(false);
            MAIN_MEMORY.getColumnModel().getColumn(0).setPreferredWidth(20);
        }

        memory_nbase_options.add(memory_nbase_BIN);
        memory_nbase_BIN.setFont(memory_nbase_BIN.getFont().deriveFont(memory_nbase_BIN.getFont().getSize() - 3f));
        memory_nbase_BIN.setText("BIN");
        memory_nbase_BIN.setName("memory_nbase_BIN");
        memory_nbase_BIN.setNextFocusableComponent(memory_nbase_DEC);
        memory_nbase_BIN.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            toggle_memory_nbase_BIN();
        });

        memory_nbase_options.add(memory_nbase_HEX);
        memory_nbase_HEX.setFont(memory_nbase_HEX.getFont().deriveFont(memory_nbase_HEX.getFont().getSize() - 3f));
        memory_nbase_HEX.setText("HEX");
        memory_nbase_HEX.setName("memory_nbase_HEX");
        memory_nbase_HEX.setNextFocusableComponent(MAIN_INPUT);
        memory_nbase_HEX.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            toggle_memory_nbase_HEX();
        });

        memory_nbase_options.add(memory_nbase_DEC);
        memory_nbase_DEC.setFont(memory_nbase_DEC.getFont().deriveFont(memory_nbase_DEC.getFont().getSize() - 3f));
        memory_nbase_DEC.setText("DEC");
        memory_nbase_DEC.setName("memory_nbase_DEC");
        memory_nbase_DEC.setNextFocusableComponent(memory_nbase_HEX);
        memory_nbase_DEC.addActionListener((java.awt.event.ActionEvent evt) ->
        {
            toggle_memory_nbase_DEC();
        });

        memory_range_text.setFont(memory_range_text.getFont().deriveFont(memory_range_text.getFont().getSize() - 4f));
        memory_range_text.setText("Display first 1000 memory positions, from");

        memory_range_first.setFont(memory_range_first.getFont().deriveFont(memory_range_first.getFont().getSize() - 4f));
        memory_range_first.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        memory_range_first.setText("0");
        memory_range_first.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout MEMORY_AREALayout = new javax.swing.GroupLayout(MEMORY_AREA);
        MEMORY_AREA.setLayout(MEMORY_AREALayout);
        MEMORY_AREALayout.setHorizontalGroup(
                MEMORY_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(title_MEMORY, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(MEMORY_AREALayout.createSequentialGroup()
                        .addGroup(MEMORY_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(MAIN_MEMORY_sp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(MEMORY_AREALayout.createSequentialGroup()
                                        .addGroup(MEMORY_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(MEMORY_AREALayout.createSequentialGroup()
                                                        .addComponent(memory_nbase_BIN)
                                                        .addGap(63, 63, 63)
                                                        .addComponent(memory_nbase_DEC))
                                                .addComponent(memory_range_text))
                                        .addGroup(MEMORY_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(MEMORY_AREALayout.createSequentialGroup()
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(memory_nbase_HEX, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(MEMORY_AREALayout.createSequentialGroup()
                                                        .addGap(3, 3, 3)
                                                        .addComponent(memory_range_first, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(1, 1, 1))
        );
        MEMORY_AREALayout.setVerticalGroup(
                MEMORY_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(MEMORY_AREALayout.createSequentialGroup()
                        .addComponent(title_MEMORY)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(MAIN_MEMORY_sp, javax.swing.GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(MEMORY_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(memory_range_text)
                                .addComponent(memory_range_first, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addGroup(MEMORY_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(memory_nbase_BIN, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(MEMORY_AREALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(memory_nbase_HEX, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(memory_nbase_DEC, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        memory_nbase_BIN.getAccessibleContext().setAccessibleName("memory_nbase_BIN");
        memory_nbase_HEX.getAccessibleContext().setAccessibleName("memory_nbase_HEX");
        memory_nbase_DEC.getAccessibleContext().setAccessibleName("memory_nbase_DEC");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                          .addContainerGap()
                          .addComponent(IO_AREA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                          .addComponent(REGISTERS_AREA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                          .addComponent(MEMORY_AREA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                          .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(IO_AREA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(REGISTERS_AREA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                .addComponent(MEMORY_AREA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
        );
    }

    private void buildGUI()
    {
        this.initializeGUIElements();

        this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        this.setTitle(SICXE.machineName + " Assembler & Simulator");
        this.setName(SICXE.machineName + " Assembler & Simulator");

        this.buildMenuBar();
        this.buildIOArea();
        this.buildRegistersArea();
        this.buildMemoryArea();

        this.pack();

        this.BUTTON_Undo.setEnabled(false);
        this.memory_nbase_DEC.setSelected(true);
        this.registers_nbase_DEC.setSelected(true);
    }

    private void shrinkRegistersContent()
    {
        register_content_A.setFont(this.register_content_A.getFont().deriveFont(register_content_A.getFont().getSize() - 4f));
        register_content_X.setFont(this.register_content_X.getFont().deriveFont(register_content_X.getFont().getSize() - 4f));
        register_content_L.setFont(this.register_content_L.getFont().deriveFont(register_content_L.getFont().getSize() - 4f));
        register_content_B.setFont(this.register_content_B.getFont().deriveFont(register_content_B.getFont().getSize() - 4f));
        register_content_S.setFont(this.register_content_S.getFont().deriveFont(register_content_S.getFont().getSize() - 4f));
        register_content_T.setFont(this.register_content_T.getFont().deriveFont(register_content_T.getFont().getSize() - 4f));
        register_content_PC.setFont(this.register_content_PC.getFont().deriveFont(register_content_PC.getFont().getSize() - 4f));
        register_content_SW.setFont(this.register_content_SW.getFont().deriveFont(register_content_SW.getFont().getSize() - 4f));
    }

    private void enlargeRegistersContent()
    {
        JTextField tempModel = new JTextField();
        this.register_content_A.setFont(tempModel.getFont());
        this.register_content_X.setFont(tempModel.getFont());
        this.register_content_L.setFont(tempModel.getFont());
        this.register_content_B.setFont(tempModel.getFont());
        this.register_content_S.setFont(tempModel.getFont());
        this.register_content_T.setFont(tempModel.getFont());
        this.register_content_PC.setFont(tempModel.getFont());
        this.register_content_SW.setFont(tempModel.getFont());
    }

    private void toggle_registers_nbase_HEX()
    {
        this.reverse_update_registers();
        this.enlargeRegistersContent();
        this.registers_nbase = 16;
        this.register_content_A.setText(SICXE.vh.toHex(SICXE.machine.register("A")));
        this.register_content_X.setText(SICXE.vh.toHex(SICXE.machine.register("X")));
        this.register_content_L.setText(SICXE.vh.toHex(SICXE.machine.register("L")));
        this.register_content_B.setText(SICXE.vh.toHex(SICXE.machine.register("B")));
        this.register_content_S.setText(SICXE.vh.toHex(SICXE.machine.register("S")));
        this.register_content_T.setText(SICXE.vh.toHex(SICXE.machine.register("T")));
        this.register_content_F.setText("0x???...");
        this.register_content_PC.setText(SICXE.vh.toHex(SICXE.machine.register("PC")));
        this.register_content_SW.setText(SICXE.vh.toHex(SICXE.machine.register("SW")));
        System.out.println("[] Numeric base set to " + this.registers_nbase + " for registers");
    }

    private void toggle_registers_nbase_DEC()
    {
        this.enlargeRegistersContent();
        this.registers_nbase = 10;
        this.register_content_A.setText(Integer.toString(SICXE.machine.register("A")));
        this.register_content_X.setText(Integer.toString(SICXE.machine.register("X")));
        this.register_content_L.setText(Integer.toString(SICXE.machine.register("L")));
        this.register_content_B.setText(Integer.toString(SICXE.machine.register("B")));
        this.register_content_S.setText(Integer.toString(SICXE.machine.register("S")));
        this.register_content_T.setText(Integer.toString(SICXE.machine.register("T")));
        this.register_content_F.setText("??.?");
        this.register_content_PC.setText(Integer.toString(SICXE.machine.register("PC")));
        this.register_content_SW.setText(Integer.toString(SICXE.machine.register("SW")));
        System.out.println("[] Numeric base set to " + this.registers_nbase + " for registers");
    }

    private void toggle_registers_nbase_BIN()
    {
        this.reverse_update_registers();
        this.shrinkRegistersContent();
        this.registers_nbase = 2;
        this.register_content_A.setText(SICXE.vh.toBin(SICXE.machine.register("A")));
        this.register_content_X.setText(SICXE.vh.toBin(SICXE.machine.register("X")));
        this.register_content_L.setText(SICXE.vh.toBin(SICXE.machine.register("L")));
        this.register_content_B.setText(SICXE.vh.toBin(SICXE.machine.register("B")));
        this.register_content_S.setText(SICXE.vh.toBin(SICXE.machine.register("S")));
        this.register_content_T.setText(SICXE.vh.toBin(SICXE.machine.register("T")));
        this.register_content_F.setText("??? ???");
        this.register_content_PC.setText(SICXE.vh.toBin(SICXE.machine.register("PC")));
        this.register_content_SW.setText(SICXE.vh.toBin(SICXE.machine.register("SW")));
        System.out.println("[] Numeric base set to " + this.registers_nbase + " for registers");
    }

    private void update_registers()
    {
        switch (this.registers_nbase)
        {
            case 2:
                this.toggle_memory_nbase_BIN();
                break;
            case 16:
                this.toggle_memory_nbase_HEX();
                break;
            default:
                this.toggle_memory_nbase_DEC();
                break;
        }
    }

    private void reverse_update_registers()
    {
        if (this.registers_nbase == 10)
        {
            SICXE.machine.setRegister("A", Integer.parseInt(this.register_content_A.getText()));
            SICXE.machine.setRegister("X", Integer.parseInt(this.register_content_X.getText()));
            SICXE.machine.setRegister("L", Integer.parseInt(this.register_content_L.getText()));
            SICXE.machine.setRegister("B", Integer.parseInt(this.register_content_B.getText()));
            SICXE.machine.setRegister("S", Integer.parseInt(this.register_content_S.getText()));
            SICXE.machine.setRegister("T", Integer.parseInt(this.register_content_T.getText()));
            SICXE.machine.setRegister("PC", Integer.parseInt(this.register_content_PC.getText()));
            SICXE.machine.setRegister("SW", Integer.parseInt(this.register_content_SW.getText()));
        }
    }

    private void toggle_memory_nbase_HEX()
    {
        this.memory_nbase = 16;
        this.blank_word = "0x000000";
        this.blank_byte = "0x00";

        String bufferA, bufferB;
        for (int i = 0; i < 1000; i++)
        {
            bufferA = "0x" + Integer.toHexString(SICXE.machine.memoryByteAt(i));
            if ((i == 0) || ((i % 3) == 0))
            {
                bufferB = "0x" + Integer.toHexString(SICXE.machine.memoryWordAt(i));
                this.MAIN_MEMORY.setValueAt((bufferA + " (" + bufferB + ")"), i, 1);
            }
            else
            {
                this.MAIN_MEMORY.setValueAt(bufferA, i, 1);
            }
        }

        System.out.println("[] Numeric base set to " + this.memory_nbase + " for main memory content");
    }

    private void toggle_memory_nbase_BIN()
    {
        this.memory_nbase = 2;
        this.blank_word = "000000000000000000000000";
        this.blank_byte = "00000000";

        String bufferA, bufferB;
        for (int i = 0; i < 1000; i++)
        {
            if ((i == 0) || ((i % 3) == 0))
            {
                bufferB = SICXE.vh.toBin(SICXE.machine.memoryWordAt(i), 24);
                this.MAIN_MEMORY.setValueAt(bufferB, i, 1);
            }
            else
            {
                bufferA = SICXE.vh.toBin(SICXE.machine.memoryByteAt(i), 8);
                this.MAIN_MEMORY.setValueAt(bufferA, i, 1);
            }
        }

        System.out.println("[] Numeric base set to " + this.memory_nbase + " for main memory content");
    }

    private void toggle_memory_nbase_DEC()
    {
        this.memory_nbase = 10;
        this.blank_word = "0";
        this.blank_byte = "0";

        String bufferA, bufferB;
        for (int i = 0; i < 1000; i++)
        {
            bufferA = Integer.toString(SICXE.machine.memoryByteAt(i));
            if ((i == 0) || ((i % 3) == 0))
            {
                bufferB = Integer.toString(SICXE.machine.memoryWordAt(i));
                this.MAIN_MEMORY.setValueAt((bufferA + " (" + bufferB + ")"), i, 1);
            }
            else
            {
                this.MAIN_MEMORY.setValueAt(bufferA, i, 1);
            }
        }

        System.out.println("[] Numeric base set to " + this.memory_nbase + " for main memory content");
    }

    private void update_memory()
    {
        String bufferA, bufferB;
        for (int pos = 0; SICXE.machine.gotMemoryModifications(); pos = SICXE.machine.modifiedMemoryPos())
        {
            if (this.memory_nbase == 10)
            {
                bufferA = Integer.toString(SICXE.machine.memoryByteAt(pos));
                bufferB = bufferA + " (" + Integer.toString(SICXE.machine.memoryWordAt(pos)) + ")";
            }
            else if (this.memory_nbase == 16)
            {
                bufferA = SICXE.vh.toBin(SICXE.machine.memoryByteAt(pos), 8);
                bufferB = bufferA + " (" + SICXE.vh.toBin(SICXE.machine.memoryWordAt(pos), 24) + ")";
            }
            else
            {
                bufferA = Integer.toHexString(SICXE.machine.memoryByteAt(pos));
                bufferB = Integer.toHexString(SICXE.machine.memoryWordAt(pos));
            }

            if ((pos == 0) || ((pos % 3) == 0))
            {
                this.MAIN_MEMORY.setValueAt(bufferB, pos - this.memory_first_pos, 1);
            }
            else
            {
                this.MAIN_MEMORY.setValueAt(bufferA, pos - this.memory_first_pos, 1);
            }
        }
    }

    private void clearMemory()
    {
        this.blank_byte = this.blank_word = "0";
        for (int i = 0; i < 1000; i++)
        {
            if ((i == 0) || ((i % 3) == 0))
            {
                this.MAIN_MEMORY.setValueAt((this.blank_byte + "(" + this.blank_word + ")"), i, 1);
            }
            else
            {
                this.MAIN_MEMORY.setValueAt(this.blank_byte, i, 1);
            }
        }

        SICXE.machine.clearMemory();

        System.out.println("## All memory positions set to zero");
    }

    private void open()
    {
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            String filePath = null;
            String buffer = null;
            try
            {
                filePath = fileChooser.getSelectedFile().getPath();
                BufferedReader reader = new BufferedReader(new FileReader(filePath));
                for (String line; (line = reader.readLine()) != null;)
                {
                    buffer += line + "\n";
                }
                reader.close();

                this.undoBuffer = this.MAIN_INPUT.getText();
                this.BUTTON_Undo.setEnabled(true);

                this.MAIN_INPUT.setText(buffer);
            }
            catch (IOException e)
            {
                Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    private void save()
    {
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                PrintWriter writer = new PrintWriter(fileChooser.getSelectedFile().getPath(), "UTF-8");
                writer.print(this.MAIN_INPUT.getText());
                writer.close();
            }
            catch (IOException e)
            {
                Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    private void restart()
    {
        this.setVisible(false);
        this.dispose();
        SICXE.restart();
    }

    private void quit()
    {
        this.setVisible(false);
        this.dispose();
    }

    private void undo()
    {
        this.MAIN_INPUT.setText(this.undoBuffer);
        this.undoBuffer = null;
        this.BUTTON_Undo.setEnabled(false);
    }

    private void expandMacrosOnInput()
    {
        this.undoBuffer = this.MAIN_INPUT.getText();
        this.BUTTON_Undo.setEnabled(true);
        this.MAIN_INPUT.setText(SICXE.macroExpander.expandMacros(this.undoBuffer));
    }

    private void assembleInput()
    {
        SICXE.assembler = new Assembler(this.MAIN_INPUT.getText());
        SICXE.assembler.assemble();
        System.out.println("** CODE ASSEMBLED:");
        SICXE.assembler.printInstructions();
        SICXE.assembler.loadToMemory();
        SICXE.machine.setRegister("PC", SICXE.assembler.getStartingAddress());

        this.update_memory();
        this.update_registers();
    }

    private void runCode()
    {
        for (int pos = SICXE.machine.PC(); pos < SICXE.mainMemoryBytes; pos = SICXE.machine.PC())
        {
            SICXE.machine.runInstruction();

            this.update_memory();
            this.update_registers();

            if (SICXE.machine.gotErrors())
            {
                for (String aux = ""; !(".".equals(aux = SICXE.machine.errorMessage()));)
                {
                    this.MAIN_OUTPUT.append(aux + "\n");
                }
                break;
            }
        }
    }

    public Interface()
    {
        this.memory_first_pos = 0;
        this.memory_last_pos = 999;

        this.undoBuffer = null;

        this.initializeFileChooser();

        this.setLookAndFeel();
        this.buildGUI();

        this.toggle_memory_nbase_DEC();
        this.toggle_registers_nbase_DEC();

        System.out.println("===[Virtual SIC/XE]================================");

        this.setVisible(true);
    }
}
